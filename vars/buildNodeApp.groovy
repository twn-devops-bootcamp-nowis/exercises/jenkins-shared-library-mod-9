#!/usr/bin/env groovy

def call() {
    dir('app') {
        echo "Building the Node.js application for branch $BRANCH_NAME"
        
        echo 'Installing dependencies...'
        sh 'npm install'

        echo 'Running tests...'
        sh 'npm test'

        echo 'Starting application...'
        sh 'npm start &'
    }
}
